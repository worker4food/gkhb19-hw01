package gkhb19.worker4food.hw01

fun CharSequence.shuffleInner() = when {
    length < 4 -> this
    else -> substring(1, length - 1)
        .asIterable()
        .shuffled()
        .joinToString("", prefix = take(1), postfix = takeLast(1))
}

fun CharSequence.mapWords(op: (MatchResult) -> CharSequence) = replace("\\p{L}+".toRegex(), op)