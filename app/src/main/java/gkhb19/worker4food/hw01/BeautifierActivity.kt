package gkhb19.worker4food.hw01

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.os.bundleOf
import kotlinx.android.synthetic.main.activity_beautifier.*
import java.util.*

class BeautifierActivity : AppCompatActivity() {
    companion object {
        const val INVALID_INPUT = Activity.RESULT_FIRST_USER + 1

        const val EXTRA_TEXT = "text"
        const val EXTRA_ERROR = "error"
    }

    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beautifier)

        if(intent.hasExtra(EXTRA_TEXT))
            message.text = intent.getStringExtra(EXTRA_TEXT)
        else
            fin(bundleOf(EXTRA_ERROR to "no text - no transformations"), INVALID_INPUT)

        capButton.setOnClickListener {
            message.text = message.text.mapWords { it.value.capitalize(Locale.US) }
        }

        shuffleButton.setOnClickListener {
            message.text = message.text.mapWords { it.value.shuffleInner() }
        }

        cancelButton.setOnClickListener {
            bundleOf(EXTRA_ERROR to "operation canceled").also(::cancel)
        }

        okButton.setOnClickListener {
            if(message.text.isBlank()) // Hello, `crashlytics`!
                throw Exception("Impossible: message.text is empty")

            bundleOf(EXTRA_TEXT to message.text).also(::ok)
        }
    }

    private fun fin(ret: Bundle, resultCode: Int) {
        Intent()
            .apply { putExtras(ret) }
            .also  { setResult(resultCode, it) }
        finish()
    }

    private fun ok(ret: Bundle) = fin(ret, Activity.RESULT_OK)

    private fun cancel(ret: Bundle) = fin(ret, Activity.RESULT_CANCELED)
}
