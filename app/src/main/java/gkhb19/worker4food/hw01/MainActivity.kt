package gkhb19.worker4food.hw01

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.os.bundleOf
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

typealias BActivity = BeautifierActivity

class MainActivity : AppCompatActivity() {
    companion object {
        const val TRANSFORM_REQUEST = 42
    }

    private val quotes: Array<String>
        get() = resources.getStringArray(R.array.quotes)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        randomButton.setOnClickListener { message.setText(randomQuote()) }

        transformButton.setOnClickListener {
            if(isValid()) Intent(this, BActivity::class.java)
                .apply { putExtra(BActivity.EXTRA_TEXT, message.text.toString()) }
                .also { startActivityForResult(it, TRANSFORM_REQUEST) }
           else
                notify(R.string.type_some_text_please)
        }

        emailButton.setOnClickListener {
            if(!isValid()) notify(R.string.type_some_text_please)
            else Intent(Intent.ACTION_SENDTO).apply {
                data = Uri.parse("mailto:")

                putExtras(bundleOf(
                    Intent.EXTRA_SUBJECT to "Very important mail",
                    Intent.EXTRA_TEXT to message.text.toString()))

                if(resolveActivity(packageManager) != null)
                    startActivity(this)
                else
                    notify("No e-mail app found!")
            }
        }
    }

    private fun randomQuote(): String {
        return quotes[Random.nextInt(quotes.size)]
    }

    private fun isValid()  = message.text.isNotBlank()

    private fun notify(msg: String) = Toast
        .makeText(this, msg, Toast.LENGTH_SHORT)
        .show()

    private fun notify(resId: Int) = resources.getString(resId).also(::notify)

    public override fun onActivityResult(reqCode: Int, resCode: Int, data: Intent?) {
        super.onActivityResult(reqCode, resCode, data)

        if(reqCode != TRANSFORM_REQUEST) return
        if(data == null) return

        when(resCode) {
            BeautifierActivity.INVALID_INPUT, Activity.RESULT_CANCELED ->
                data.extras?.getString(BActivity.EXTRA_ERROR)!!.also(::notify)

            Activity.RESULT_OK ->
                data.extras?.getString(BActivity.EXTRA_TEXT)!!.also(message::setTextKeepState)
        }
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString("message", message.text.toString())
        outState.putInt("selectionStart", message.selectionStart)
        outState.putInt("selectionEnd", message.selectionEnd)
    }

    public override fun onRestoreInstanceState(state: Bundle?) {
        super.onRestoreInstanceState(state)

        state?.getString("message")?.also(message::setText)
        state?.getInt("selectionStart")?.also { start ->
            val end = state.getInt("selectionEnd")
            message.setSelection(start, end)
        }
    }
}
